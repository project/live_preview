/**
 * @file
 * Script for the iframe part of live-preview.
 */
(function ($) {

  /**
   *
   */
  Drupal.behaviors.livePreviewIframe = {
    attach: function (context, settings) {
      //
      $('body').once('live-preview-iframe', function() {
        var $body = $(this);

        // Prevent default behaviour for all links and buttons
        var blockSelectors = 'a,button,input[type="submit"],input[type="button"]';
        $(blockSelectors, $body).once('live-preview', function() {
          $(this).click(function(e) {
            e.preventDefault();

            return false;
          });
        });
      });

      // This will only run once per page load.
      $('html').once('live-preview-iframe', function() {
        var $html = $(this);

        // Re-attach behaviours when we detect a new body element
        var reAttach = function() {
          if (!$('body').hasClass('live-preview-iframe-processed')) {
            Drupal.attachBehaviors();
          }
          setTimeout(reAttach, 200);
        };
        reAttach();
      });
    }
  };

})(jQuery);