/**
 * @file
 * Front-end for live-preview.
 */
(function ($) {
  Drupal.LivePreview = Drupal.LivePreview || {};
  Drupal.LivePreview.Viewer = function() { };

  /**
   *
   */
  $.extend(Drupal.LivePreview.Viewer.prototype, {
    isOpen: false,
    paused: false,
    ajaxing: false,
    iframeUpdateTimeout: null,
    iframeInitialized: false,
    iframeUpdateCount: 0,

    $overlay: null,
    $editor: null,
    $iframeContainer: null,
    $iframe: null,
    $editorHeader: null,
    $closeButton: null,
    $updateButton: null,
    $messages: null,

    /**
     *
     * @param settings
     */
    init: function(settings) {
      var opts = $.extend({}, settings);

      this.$form = $(opts.form);
      this.iframe_id = opts.iframe_id;
      this.$previewBtn = $(opts.preview_btn);

      this.fieldSettings = opts.fields;

      this.$body = $('body');
    },

    /**
     *
     */
    open: function() {
      if (this.isOpen) {
        return;
      }

      if (!this.$editor) {
        // Prepare containers and required markup
        this.$overlay = $('<div class="live-preview-overlay"></div>').appendTo(this.$body);
        this.$iframeContainer = $('<div class="live-preview-iframe-container"></div>').appendTo(this.$body);
        this.$iframe = $('<iframe class="live-preview-iframe" frameborder="0"></iframe>').attr('id', this.iframe_id).appendTo(this.$iframeContainer);

        this.$editorHeader = $('<div class="live-preview-header"></div>').prependTo(this.$form);
        this.$closeButton = $('<a href="#" class="button live-preview-close">' + Drupal.t('Close') + '</a>').appendTo(this.$editorHeader);
        this.$updateButton = $('<a href="#" class="button live-preview-refresh">' + Drupal.t('Update') + '</a>').appendTo(this.$editorHeader);
        $('<h2>' + Drupal.t('Live preview') + '</h2>').appendTo(this.$editorHeader);

        this.$messages = $('<div id="live-preview-messages"></div>').appendTo(this.$editorHeader);

        // Listen for updates from our AJAX callback (triggered by the 'live_preview_set' ajax command)
        var _this = this;
        this.$iframe.bind('live-preview-update', $.proxy(this.onIframeUpdate, this));
        this.$closeButton.bind('click', function(e) { _this.close(); return false; });
        this.$updateButton.bind('click', function(e) { _this.updateIframe(); return false; });
      }

      this.fields = [];
      for (var id in this.fieldSettings) {
        var $field = $(id),
            $wrapper = $field.parentsUntil('form', '.form-item');
        $.extend(this.fieldSettings[id], {
          field: $field[0],
          wrapper: $wrapper[0]
        });
        var $el = $wrapper.length ? $wrapper : $field;
        $el.data('settings', this.fieldSettings[id]);
        $el.data('id', id);
        $el.data('weight', parseInt(this.fieldSettings[id]['weight']));

        this.fields.push($el[0]);
      }
      $(this.fields).first().parent().children().each(function() {
        $(this).data('originalWeight', $(this).index());
      });

      //Drupal.attachBehaviors(this.$editor, null);

      this.updateIframe();

      this.enable();

      this.isOpen = true;
    },

    /**
     *
     * @param key
     */
    sortFields: function(key) {
      Drupal.detachBehaviors(this.$form);

      $(this.fields).first().parent().children().sortElements(function(a, b) {
        var wa = ($(a).data(key) || -9999),
            wb = ($(b).data(key) || -9999);
        return wa > wb ? 1 : -1;
      });

      Drupal.attachBehaviors(this.$form);
    },

    /**
     *
     */
    enable: function() {
      $('html,body').addClass('live-preview-active');

      var _this = this;

      // Show iframe and apply altered layout to the form
      this.$overlay.show().addClass('open');
      this.$iframeContainer.show();

      this.$form.addClass('live-preview-form-layout opening');
      setTimeout(function() {
        _this.$form.removeClass('opening');
      }, 1200);

      // Mark chosen fields as visible
      $.each(this.fields, function() {
        var $el = $(this);
        $el.data('settings')['originalWeight'] = $el.index();
        $el.addClass('live-preview-visible');
      });

      // Re-order fields
      this.sortFields('weight');

      var $form = this.$form;
      // Collects all elements that aren't set explicitly visible via the form settings
      var hiddenEls = $('> div *:not(.live-preview-visible)', $form)
        .filter(function() {
          return $(this).parentsUntil('form', '.live-preview-visible').length === 0 &&
            $(this).find('.live-preview-visible').length === 0;
        });
      hiddenEls.addClass('live-preview-hidden-trail--temp');

      // Reduce the collection to the most top level elements
      // @TODO: Optimize both this and the above filter. There's got to be a better way!
      hiddenEls = hiddenEls.filter(function(i, el) {
        var $this = $(this);
        if ($this.parents('.live-preview-hidden-trail--temp').last().length === 0) {
          $this.find('*').removeClass('live-preview-hidden-trail--temp');
          return true;
        }
        return false;

        // jQuery >= 1.6:
        // return $(this).closest(hiddenEls.not($(this))).length === 0;
      });
      hiddenEls = this.$form.find('.live-preview-hidden-trail--temp');

      // Hide all markup which isn't used with live preview
      setTimeout(function() {
        hiddenEls
          .addClass('live-preview-hidden-trail')
          .removeClass('live-preview-hidden-trail--temp');
      }, 800);

      // 
      $(this).one('load', function(e, init) {
        _this.$iframeContainer.addClass('open');
      });

      // Listen for changes to trigger refresh of iframe
      // TODO: figure a way to do this reliably, as we can't listen to all input fields
      //$('input, textarea', $form).on('keyup change', $.proxy(this._scheduleIframeUpdate, this, 500));

      // Listen for ESC
      this.$body.bind('keyup', $.proxy(this._onKeyUp, this));

      this._visibilityBind();
    },

    /**
     *
     */
    close: function() {
      if (!this.isOpen) {
        return;
      }

      var _this = this;

      this.$overlay.removeClass('open');
      this.$iframeContainer.removeClass('open');
      this.$form.addClass('closing');
      setTimeout(function() {
        _this.$form.removeClass('live-preview-form-layout closing');
        $('html,body').removeClass('live-preview-active');
      }, 800);

      //
      this.$form.find('.live-preview-visible').removeClass('live-preview-visible');
      this.$form.find('.live-preview-hidden-trail').removeClass('live-preview-hidden-trail');

      this.$body.unbind('keyup', $.proxy(this._onKeyUp, this));
      //$('input, textarea', this.$form).off('keyup change', $.proxy(this._scheduleIframeUpdate, this, 500));

      if (this.iframeUpdateTimeout) {
        clearTimeout(this.iframeUpdateTimeout);
      }
      this.iframeInitialized = false;

      var els = [this.$overlay, this.$iframeContainer, this.$iframe, this.$editorHeader].map(function(e) { return e[0]; });
      setTimeout(function() {
        _this.sortFields('originalWeight');
        _this.$overlay.hide();
        _this.$iframeContainer.hide();
        $(els).detach();
      }, 800);

      this.isOpen = false;
    },

    /**
     * @see http://api.jquery.com/jQuery.parseHTML/
     */
    parseHTML: function(data, context, scripts) {
      var rsingleTag = /^<(\w+)\s*\/?>(?:<\/\1>|)$/;
      var parsed;
      if ( !data || typeof data !== "string" ) {
        return null;
      }
      if ( typeof context === "boolean" ) {
        scripts = context;
        context = 0;
      }
      context = context || document;

      // Single tag
      if ( (parsed = rsingleTag.exec( data )) ) {
        return [ context.createElement( parsed[1] ) ];
      }

      parsed = $.buildFragment( [ data ], context, scripts ? null : [] );
      return $.merge( [],
        (parsed.cacheable ? $.clone( parsed.fragment ) : parsed.fragment).childNodes );
    },

    /**
     *
     */
    updateIframe: function() {
      // Page is not active; no need to update
      if (this.paused || this.ajaxing) {
        return;
      }
      this.ajaxing = true;
      this.$previewBtn.click();
    },

    /**
     *
     */
    onIframeUpdate: function(event, data) {
      var _this = this,
          settings = data.settings;

      this.ajaxing = false;

      // Initial load. Populate iframe document in a rather raw manner.
      if (!this.iframeInitialized) {
        var iframeDoc = this.$iframe[0].contentDocument || this.$iframe[0].contentWindow.document;
        iframeDoc.write(data.page);
        iframeDoc.close();

        this.iframeUpdateCount = 0;
        this.$iframe.one('load', function(event) {
          _this.iframeInitialized = true;
          $(_this).trigger('load', true);
          _this.iframeUpdateCount++;
        });
        this._scheduleIframeUpdate(2000);

      // Subsequent updates uses jQuery to parse the HTML, and then append new body
      //  HTML to the existing body.
      } else {
        var html = this.parseHTML(data.page, this.$iframe.contents(), false);
        var body = $(html).filter(function() {
          return [undefined,'TITLE','STYLE','META','LINK'].indexOf(this.tagName) === -1;
        });

        // Replace body
        // @TODO: perform "smart" replacement based on diff and data from the form submit callback, so we
        // won't have to replace the entire body every time (get's pretty expensive for big sites with alot
        // of rich content).
        this.$iframe.contents().find('body').html(body);

        // Optionally replace node wrapper with diffed markup
        if (settings['show_diff'] && data.diff) {
          var diffedContent = this.parseHTML(data.diff, body, false);

          body.find('.live-preview-node-wrapper').replaceWith(diffedContent);
        }

        $(this).trigger('load');
        this.iframeUpdateCount++;

        // Update every 2 seconds
        this._scheduleIframeUpdate(200);
      }
    },

    _onVisibilityChange: function(hidden) {
      this.paused = !!hidden;
    },

    _visibilityBind: function() {
      if (this.visibilityBindDone) {
        return;
      }
      this.visibilityBindDone = true;

      var hidden = 'hidden',
          onVisibilityChange = this._onVisibilityChange.bind(this);

      // Standards:
      if (hidden in document)
        document.addEventListener('visibilitychange', onchange);
      else if ((hidden = 'mozHidden') in document)
        document.addEventListener('mozvisibilitychange', onchange);
      else if ((hidden = 'webkitHidden') in document)
        document.addEventListener('webkitvisibilitychange', onchange);
      else if ((hidden = 'msHidden') in document)
        document.addEventListener('msvisibilitychange', onchange);
      // IE 9 and lower:
      else if ('onfocusin' in document)
        document.onfocusin = document.onfocusout = onchange;
      // All others:
      else
        window.onpageshow = window.onpagehide
          = window.onfocus = window.onblur = onchange;

      function onchange(evt) {
        var v = 'visible', h = 'hidden', s = '',
          evtMap = {
            focus: v, focusin: v, pageshow: v, blur: h, focusout: h, pagehide: h
          };

        evt = evt || window.event;
        if (evt.type in evtMap)
          s = evtMap[evt.type];
        else
          s = this[hidden] ? 'hidden' : 'visible';

        onVisibilityChange(s==='hidden');
      }

      // set the initial state (but only if browser supports the Page Visibility API)
      if (document[hidden] !== undefined)
        onchange({type: document[hidden] ? 'blur' : 'focus'});
    },

    /**
     *
     * @param delay
     * @private
     */
    _scheduleIframeUpdate: function(delay) {
      delay = delay || 1000;
      if (this.iframeUpdateTimeout) {
        clearTimeout(this.iframeUpdateTimeout);
      }
      this.iframeUpdateTimeout = setTimeout($.proxy(this.updateIframe, this), delay);
    },

    /**
     *
     */
    _onKeyUp: function(e) {
      // ESC
      if (e.keyCode === 27) {
        this.close();
      }
    }
  });


  /**
   *
   * @type {{attach: Function}}
   */
  Drupal.behaviors.livePreview = {
    attach: function (context, settings) {
      if (!settings.live_preview) {
        return;
      }
      var opts = settings.live_preview,
          $form = $(opts.form);

      $form.once('live-preview', function() {
        var livePreview = new Drupal.LivePreview.Viewer(opts);
        livePreview.init(opts);

        $(opts.trigger).click(function(e) {
          e.preventDefault();

          livePreview.open();
          return false;
        });
      });
    }
  };


  /**
   *
   */
  Drupal.ajax.prototype.commands.live_preview_set = function(ajax, response, status) {
    var iframe = response.selector ? $(response.selector) : $(ajax.wrapper);

    iframe.trigger('live-preview-update', {
      page: response.data,
      diff: response.diff,
      settings: response.settings
    });
  };



  /**
   * Page visibility API, with fallback to focus/blur
   */
  Drupal.behaviors.pageVisibilityAPI = {
    attach: function (context, settings) {
      $('body').once('visibility-api', function() {

        (function() {
        })();

      });
    }
  };


  /**
   * jQuery.fn.sortElements
   *
   * http://james.padolsey.com/javascript/sorting-elements-with-jquery/
   */
  $.fn.sortElements = (function(){
    var sort = [].sort;

    return function(comparator, getSortable) {
      getSortable = getSortable || function(){return this;};
      var placements = this.map(function(){
        var sortElement = getSortable.call(this),
          parentNode = sortElement.parentNode,

        // Since the element itself will change position, we have
        // to have some way of storing its original position in
        // the DOM. The easiest way is to have a 'flag' node:
          nextSibling = parentNode.insertBefore(
            document.createTextNode(''),
            sortElement.nextSibling
          );
        return function() {
          if (parentNode === this) {
            throw new Error(
              "You can't sort elements if any one is a descendant of another."
            );
          }

          // Insert before flag:
          parentNode.insertBefore(this, nextSibling);
          // Remove flag:
          parentNode.removeChild(nextSibling);
        };
      });

      return sort.call(this, comparator).each(function(i){
        placements[i].call(getSortable.call(this));
      });
    };
  })();

})(jQuery);