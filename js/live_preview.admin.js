(function ($) {
  Drupal.behaviors.livePreviewAdmin = {
    attach: function (context) {

      var $submission_fieldset = $('fieldset#edit-submission');

      // Provide the vertical tab summaries.
      $('fieldset#edit-live-preview', context).drupalSetSummary(function(context) {
        var vals = [];

        if ($('#edit-node-preview-0', $submission_fieldset).is(':checked')) {
          vals.push(Drupal.t('Not available'));
        } else if (!$('#edit-live-preview-enable', context).is(':checked')) {
          vals.push(Drupal.t('Not enabled'));
        } else {
          vals.push(Drupal.t('Enabled'));
        }

        return vals.join(', ');
      });

      $submission_fieldset.bind('summaryUpdated', function() {
        $('fieldset#edit-live-preview', context).trigger('summaryUpdated');
      });

      $('a[href="#edit-submission"]', context).click(function(e) {
        e.preventDefault();
        $submission_fieldset.data('verticalTab').focus();
        return false;
      });
    }
  };


  Drupal.behaviors.livePreviewAdminFieldUI = {
    attach: function (context, settings) {
      $('table#live-preview-fields', context).once('live-preview-admin', function() {

        Drupal.fieldUIOverview.attach(this, settings.fieldUIRowsData, Drupal.fieldUIDisplayOverview);
      });
    }
  };


  Drupal.livePreviewAdminFieldUI = {
    onChange: function() {
      var $trigger = $(this);
      var row = $trigger.closest('tr').get(0);
      var rowHandler = $(row).data('fieldUIRowHandler');

      var refreshRows = {};
      refreshRows[rowHandler.name] = $trigger.get(0);

      // Handle region change.
      var region = rowHandler.getRegion();
      if (region != rowHandler.region) {
        // Let the row handler deal with the region change.
        $.extend(refreshRows, rowHandler.regionChange(region));
        // Update the row region.
        rowHandler.region = region;
      }

      // Ajax-update the rows.
      Drupal.fieldUIOverview.AJAXRefreshRows(refreshRows);
    }
  };

  Drupal.fieldUIDisplayOverview.live_preview_field = function (row, data) {
    this.row = row;
    this.name = data.name;
    this.region = data.region;
    this.tableDrag = data.tableDrag;

    // Attach change listener to the 'formatter type' select.
    this.$enableSelect = $('select.field-enable-select', row);
    this.$enableSelect.change(Drupal.livePreviewAdminFieldUI.onChange);

    return this;
  };

  Drupal.fieldUIDisplayOverview.live_preview_field.prototype = {
    getRegion: function () {
      return (this.$enableSelect.val() == 'hidden') ? 'hidden' : 'visible';
    },

    regionChange: function (region) {

      this.$enableSelect.val(region);

      var refreshRows = {};
      refreshRows[this.name] = this.$enableSelect.get(0);

      return refreshRows;
    }
  };

})(jQuery);