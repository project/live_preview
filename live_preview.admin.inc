<?php

/**
 * @file
 * Live preview module administration
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function live_preview_form_node_type_form_alter(&$form, &$form_state, $form_id) {
  $node_type =& $form['#node_type'];

  $stored_settings = live_preview_get_settings('node', $node_type->type);

  $form['live_preview'] = array(
    '#type' => 'fieldset',
    '#title' => t('Live preview'),
    '#group' => 'additional_settings',
    '#collapsible' => TRUE,
    '#tree' => TRUE,
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'live_preview') . '/js/live_preview.admin.js',
      ),
    ),
  );
  $form['live_preview']['disabled'] = array(
    '#type' => 'fieldset',
    '#title' => t('Disabled'),
    '#states' => array(
      'visible' => array(
        ':input[name="node_preview"]' => array('value' => DRUPAL_DISABLED),
      ),
    ),
    'info' => array(
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' =>
        t(
          'Node preview needs to be enabled in order to use live preview.<br>' .
          'It can be enabled in the <a href="#edit-submission" title="Submission form settings">Submission form settings</a>.'
        ),
    ),
  );
  $form['live_preview']['enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable'),
    '#default_value' => $stored_settings['enable'],
    '#description' => t('Make it possible to edit this content type with live preview.'),
    '#states' => array(
      'invisible' => array(
        ':input[name="node_preview"]' => array('value' => DRUPAL_DISABLED),
      ),
    ),
  );
  $form['live_preview']['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#states' => array(
      'visible' => array(
        ':input[name="node_preview"]' => array('!value' => DRUPAL_DISABLED),
        ':input[name="live_preview[enable]"]' => array('checked' => TRUE),
      ),
    ),
  );
  $settings =& $form['live_preview']['settings'];

  $settings['view_mode'] = array(
    '#type' => 'select',
    '#title' => t('Default view mode'),
    '#default_value' => $stored_settings['view_mode'],
    '#description' => t('The content is rendered in the live preview mode using this view mode.'),
    '#options' => array(),
  );
  $entity_info = entity_get_info('node');
  $options = array();
  if (!empty($entity_info['view modes'])) {
    foreach ($entity_info['view modes'] as $view_mode => $view_mode_settings) {
      $settings['view_mode']['#options'][$view_mode] = $view_mode_settings['label'];
    }
  }

  $settings['fields'] = _live_preview_admin_field_settings('node', $node_type->type, $stored_settings['fields']);
  $settings['fields']['#prefix'] = '<div id="live-preview-fields-wrapper">';
  $settings['fields']['#suffix'] = '</div>';

  $form['refresh_rows'] = array('#type' => 'hidden');
  $form['refresh'] = array(
    '#type' => 'submit',
    '#value' => t('Refresh'),
    '#op' => 'refresh_table',
    '#submit' => array('live_preview_admin_field_settings_submit'),
    '#ajax' => array(
      'callback' => 'live_preview_admin_field_settings_ajax_cb',
      'wrapper' => 'live-preview-fields-wrapper',
      'effect' => 'fade',
      'progress' => 'none',
    ),
    '#attributes' => array('class' => array('element-invisible')),
  );

  $form['#submit'][] = 'live_preview_admin_field_settings_submit';
}

/**
 * Form submission handler for refresh button live_preview_form_node_type_form_alter()
 * and the node_type_form itself.
 *
 * @see live_preview_form_node_type_form_alter()
 */
function live_preview_admin_field_settings_submit($form, &$form_state) {
  $trigger = $form_state['triggering_element'];
  $op = isset($trigger['#op']) ? $trigger['#op'] : '';

  if ($op == 'refresh_table') {
    $form_state['rebuild'] = TRUE;
  } else {
    $values =& $form_state['values']['live_preview'];
    $bundle = $form_state['values']['type'];

    $field_instances = field_info_instances('node', $bundle);
    $extra_fields = field_info_extra_fields('node', $bundle, 'form');

    $settings = $values['settings'];
    $settings['enable'] = ($values['enable'] && $form_state['values']['node_preview'] !== DRUPAL_DISABLED);
    foreach ($settings['fields'] as $field_name => $field_settings) {
      if (isset($field_instances[$field_name])) {
        $field_instances[$field_name]['settings']['live_preview'] = $field_settings;
        field_update_instance($field_instances[$field_name]);
        $settings['fields'][$field_name]['label'] = $field_instances[$field_name]['label'];
      } else if (isset($extra_fields[$field_name])) {
        $settings['fields'][$field_name]['label'] = $extra_fields[$field_name]['label'];
      }
    }
    live_preview_set_settings($settings, 'node', $bundle);
  }
}

/**
 * Ajax handler for the field settings form.
 *
 * @see live_preview_form_node_type_form_alter()
 */
function live_preview_admin_field_settings_ajax_cb($form, &$form_state) {
  $trigger = $form_state['triggering_element'];
  $op = $trigger['#op'];

  $updated_rows = array();

  switch ($op) {
    case 'refresh_table':
      $updated_rows = array_values(explode(' ', $form_state['values']['refresh_rows']));
      break;
  }

  foreach ($updated_rows as $name) {
    $element = &$form['live_preview']['settings']['fields'][$name]['label'];
    $element['#prefix'] = '<div class="ajax-new-content">' . (isset($element['#prefix']) ? $element['#prefix'] : '');
    $element['#suffix'] = (isset($element['#suffix']) ? $element['#suffix'] : '') . '</div>';
  }

  return $form['live_preview']['settings']['fields'];
}

/**
 * Returns a form element array with all field instances on the bundle.
 *
 * @param string $entity_type
 *   Entity type
 * @param string $bundle
 *   Bundle name
 * @param array $settings
 *   (optional) The field settings array from live_preview_get_settings()
 *
 * @see live_preview_form_node_type_form_alter()
 * @ingroup forms
 */
function _live_preview_admin_field_settings($entity_type, $bundle, $fields = array()) {
  $element = array(
    '#prefix' => '<div id="live-preview-admin-fields-table">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
    '#theme' => 'live_preview_field_settings_table',
  );

  foreach ($fields as $field_name => $field_settings) {
    $element[$field_name] = array(
      'enable' => array(
        '#type' => 'select',
        '#options' => array(
          'hidden' => t('Hidden'),
          'visible' => t('Visible'),
        ),
        '#default_value' => $field_settings['enable'],
        '#attributes' => array('class' => array('field-enable-select')),
      ),
      'label' => array(
        '#type' => 'item',
        '#markup' => $field_settings['label'],
      ),
      'weight' => array(
        '#type' => 'textfield',
        '#default_value' => $field_settings['weight'],
        '#size' => 3,
        '#attributes' => array('class' => array('row-weight')),
      ),
    );
  }

  return $element;
}

/**
 * Returns HTML for the field settings table.
 *
 * @param array $variables
 *
 * @see _live_preview_admin_field_settings()
 * @ingroup themeable
 */
function theme_live_preview_field_settings_table($variables) {
  $element = $variables['element'];
  drupal_add_tabledrag('live-preview-fields', 'order', 'sibling', 'row-weight');

  $header = array(
    'label' => t('Field label'),
    'enable' => t('Visible'),
    'weight' => t('Weight'),
  );

  $js_settings = array();
  $regions = array(
    'visible' => array(
      'rows' => array(
        'message' => array(
          'class' => array('region-message', 'region-visible-message'),
          'no_striping' => TRUE,
          'data' => array(
            array('data' => t('No visible fields'), 'colspan' => count($header)),
          ),
        ),
      ),
      'children' => array(),
    ),
    'hidden' => array(
      'rows' => array(
        'title' => array(
          'class' => array('region-title', 'region-hidden-title'),
          'no_striping' => TRUE,
          'data' => array(
            array('data' => t('Hidden'), 'colspan' => count($header)),
          ),
        ),
        'message' => array(
          'class' => array('region-message', 'region-hidden-message'),
          'no_striping' => TRUE,
          'data' => array(
            array('data' => t('No hidden fields'), 'colspan' => count($header)),
          ),
        ),
      ),
      'children' => array(),
    ),
  );

  foreach (element_children($element) as $field_name) {
    $enable =& $element[$field_name]['enable'];
    $weight =& $element[$field_name]['weight'];
    $region = isset($enable['#value']) ? $enable['#value'] : $enable['#default_value'];

    $row = array('data' => array());
    foreach ($header as $key => $title) {
      $cell = array('data' => drupal_render($element[$field_name][$key]));
      $row['data'][] = $cell;
    }

    // Add row id and associate JS settings.
    $id = drupal_html_class($field_name);
    $row['id'] = $id;
    $row['class'] = array('draggable');
    $row['weight'] = isset($weight['#value']) ? (int)$weight['#value'] : (int)$weight['#default_value'];
    $row['#js_settings'] = array(
      'rowHandler' => 'live_preview_field',
      'name' => $field_name,
      'region' => $region,
    );
    $js_settings[$id] = $row['#js_settings'];

    $regions[$region]['children'][] = $row;
  }

  $rows = array();
  foreach ($regions as $key => $region) {
    if (empty($region['children'])) {
      $region['rows']['message']['class'][] = 'region-empty';
    } else {
      $region['rows']['message']['class'][] = 'region-populated';
    }
    uasort($region['children'], 'drupal_sort_weight');

    $rows = array_merge($rows, array_values($region['rows']), $region['children']);
  }

  $field_ui_path = drupal_get_path('module', 'field_ui');

  drupal_add_css($field_ui_path . '/field_ui.css');
  drupal_add_js($field_ui_path . '/field_ui.js');
  drupal_add_js(array('fieldUIRowsData' => $js_settings), 'setting');

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'live-preview-fields',
      'class' => array('field-ui-overview'),
    ),
  ));
}